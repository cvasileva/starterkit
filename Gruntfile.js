module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    concat: {
      vendor: {
        src: [ 'js/libs/*.js'],
        dest: 'assets/js/vendor.js',
        /*
         * If you decide to use the bower
          vendor : {
            src: ['js/libs/modernizr/modernizr.js', 'js/libs/jquery/jquery.js'],
            dest: 'assets/js/vendor.js'
          }
         * */
      },
      main: {
        src: ['js/*.js'],
        dest: 'assets/js/main.js',
      }
    },

    uglify: {
      main: {
        src: 'assets/js/main.js',
        dest: 'assets/js/main.min.js'
      },
      app: {
        src: ['assets/js/vendor.js'],
        dest: 'assets/js/vendor.min.js',
      }
    },

    compass: {
      deploy: {
        options: {
          imagesDir: '',
          sassDir: 'sass/',
          cssDir: 'assets/styles/',
          relativeAssets: true,
          outputStyle: 'expanded'
        }
      }
    },

    watch: {
      scripts: {
        files: ['js/**/*.js'],
        tasks: ['concat', 'uglify'],
        options: {
          spawn: false
        }
      },
      styles: {
        files: ['sass/**/*.{sass, scss}'],
        tasks: ['compass:deploy'],
      },
      templates: {
        files: ['templates/**/*'],
        tasks: ['assemble'],
      }
    },


    jshint: {
      all: [
        'Gruntfile.js', 
        'assets/js/global.js'
      ]
    },

    assemble: {
      options: {
        layout: 'templates/layouts/default.hbs',
        partials: 'templates/partials/**/*.hbs',
        flatten: true,
        data: 'templates/json/**/*.json' 
      },
      pages: {
        files: {
          './': ['templates/pages/**/*.hbs']
        }
      }
    }

  });

  // 3. Where we tell Grunt we plan to use this plug-in.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('assemble');


  // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
  grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'compass:deploy', 'assemble']);

};