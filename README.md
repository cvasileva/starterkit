## Intro

This is a starter project for a base website. It includes base sass styles, grunt build tool and template engine system + optional bower tool. Read how to install and run below.

Description:

1) Sass folder:
  * "core folder" includes core styles. I don't recommend changing anything there, as those are base styles for normalise and print.
  * "mixins folder" includes mixins for everyday use. The most commonly used are css3 mixins
  * "app folder" includes snippets of code that pertains to the ui. The base layout styles go in layout, the ui separation goes into the ui folder. I usually have a hbs "partial" and have the same sass file in the ui, just to keep things nice and tidy (ex _ui-button.hbs and _ui-button.sass).

2) Templates
  * json - where the info for the hbs comes for. Put it as a second parameter when you include the .hbs, so it knows where to read its data. Ex: {{> header common}} header is the hba, common is the json.
  * layouts/default - the base wrapping code
  * partilas -the includes. Separate them logically into folders, but you don't need to specify it's path. The assemble.io takes care of that for us.

3) Other files:
  * bower.json - if you decide to use it, put the library dependansis in here. You can do it manually, or via the terminal. If you decide manually, look at the code and copy the exaple with the jquery and moderniser. If you want to do it via the console: 'cd to/project/path' and run a command like this one ex: bower install jquery --save and it will automatically update your file with latest.
  * Gruntfile.js - does build for you. Has those base tasks: 'jshint', 'concat', 'uglify', 'compass:deploy', 'assemble'. Jshint debugs your js for you. It sniffes json and js files. Concat concatinates the js into two files. Vendor containes the libraries and main containes your scripts. !note: is you use the bower you must edit the "concat" task a bit. You need to specify the order of the concat vendor files and their paths seperately. Uglify minifies the compiled js. Compass turns sass to css. Assemble turn templates into html.
  * package.json - has ref to all npm modules Grunt uses. It you install via the terminal like ('npm install assemble.io') it will update automatically.
  * gitignore - ignores some basic files

## Code Example

  I have left examples in the code for both sass and hbs, so dig it. 

## Motivation

 Because it saves you time :):):)

## Installation

 cd path/to/project
 * install grunt. See how in http://gruntjs.com/getting-started
 * run "npm install" to install the grunt modules. They will appear in a folder "node_modules"
  ### If you decide to work with bower ###
 * install libraries: "bower install". They will appear in js/libs. Don't forget to update the Gruntfile. I have commented an example of how to do it in the concat function in the Grunt file.

## Contributors

Just me for now :). Chris Vasileva

## License

Mit License :D

## End

If you need any help, drop me a line on vasileva_ch@live.com